vim9script

export const SEP = has("win32") ? '\' : '/'

export const RTP = expand("<sfile>:p:h:h:h:h")

export def Format(path: string): string
    if empty(split(path))
        throw "tui-util-path_Format: the given path cannot be an empty string."
    endif

    final sub_paths = []
    for sub_path in split(path, '[/\\]')
        if empty(split(sub_path))
            continue
        endif
        add(sub_paths, sub_path)
    endfor
    if empty(sub_paths)
        throw "tui-util-path_Format: invalid path - [" .. path .. "]"
    endif

    if match(path, '^[/\\]') != -1
        return SEP .. join(sub_paths, SEP)
    else
        return expand(join(sub_paths, SEP))
    endif
enddef

export def Join(base_path: string, sub_paths: list<string>): string
    if empty(split(base_path))
        throw "tui-util-path_Join: the base path cannot be an empty string."
    endif

    final new_sub_paths = [base_path]
    if !empty(sub_paths)
        for sub in sub_paths
            if empty(split(sub))
                continue
            endif
            add(new_sub_paths, sub)
        endfor
    endif
    return Format(join(new_sub_paths, SEP))
enddef

## Test
# echo Format('/a\b/c//d\\e')
# echo Format('~/.vim')
# echo Format('$HOME/.vim')
# echo Join('~', ['.vim', 'init.vim'])

