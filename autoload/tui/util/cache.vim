vim9script

export const KEY_REGEX: string = '\v^[A-Za-z0-9_-]+(\.[A-Za-z0-9_-]+)*$'

export def Has(k: string): bool
    if !exists("g:tui_cached") || empty(g:tui_cached) || match(k, KEY_REGEX) == -1
        return 0
    endif

    var temp_keys = split(k, '\.')
    var temp_key  = temp_keys[-1]
    var temp_dict = g:tui_cached
    var index = 0
    for sub_key in temp_keys
        if index < len(temp_keys) - 1
            if !has_key(temp_dict, sub_key)
                return 0
            endif
            temp_dict = temp_dict[sub_key]
            if type(temp_dict) != type({})
                return 0
            endif
        endif
        index = index + 1
    endfor
    return has_key(temp_dict, temp_key)
enddef

export def Get(k: string, v: any = v:none): any
    if !exists("g:tui_cached") || empty(g:tui_cached) || match(k, KEY_REGEX) == -1
        return v
    endif

    var temp_keys = split(k, '\.')
    var temp_key  = temp_keys[-1]
    var temp_dict = g:tui_cached
    var index = 0
    for sub_key in temp_keys
        if index < len(temp_keys) - 1
            if !has_key(temp_dict, sub_key)
                return v
            endif
            temp_dict = temp_dict[sub_key]
            if type(temp_dict) != type({})
                return v
            endif
        endif
        index = index + 1
    endfor
    return has_key(temp_dict, temp_key) ? temp_dict[temp_key] : v
enddef

export def Set(k: string, v: any)
    if match(k, KEY_REGEX) == -1
        throw "tui-util-cache_Set: invalid key - [" .. k .. "]"
    endif
    if !exists("g:tui_cached")
        final g:tui_cached = {}
    endif

    var temp_keys = split(k, '\.')
    var temp_key  = temp_keys[-1]
    var temp_dict = g:tui_cached
    var index = 0
    for sub_key in temp_keys
        if index < len(temp_keys) - 1
            if !has_key(temp_dict, sub_key)
                temp_dict[sub_key] = {}
            endif
            temp_dict = temp_dict[sub_key]
            if type(temp_dict) != type({})
                throw "tui-util-cache_Set: invalid key - [" .. k .. "]"
            endif
        endif
        index = index + 1
    endfor
    temp_dict[temp_key] = v
enddef

export def Del(k: string): any
    if !exists("g:tui_cached") || empty(g:tui_cached) || match(k, KEY_REGEX) == -1
        return v:none
    endif

    var temp_keys = split(k, '\.')
    var temp_key  = temp_keys[-1]
    var temp_dict = g:tui_cached
    var index = 0
    for sub_key in temp_keys
        if index < len(temp_keys) - 1
            if !has_key(temp_dict, sub_key)
                return v:none
            endif
            temp_dict = temp_dict[sub_key]
            if type(temp_dict) != type({})
                throw "tui-util-cache_Set: invalid key - [" .. k .. "]"
            endif
        endif
        index = index + 1
    endfor
    return remove(temp_dict, temp_key)
enddef

