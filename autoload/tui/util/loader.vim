vim9script

export def Load(path: string)
    if filereadable(path)
        execute("source " .. path)
    elseif isdirectory(path)
        if filereadable(path .. "/init.vim")
            execute("source " .. path .. "/init.vim")
        else
            for file in split(glob(path .. "/*.vim"), '\n')
                if filereadable(file)
                    execute("source " .. file)
                endif
            endfor
        endif
    else
        throw "tui-util-loader_Load: the path must be a file or a directory"
    endif
enddef

## test
# Load("/home/jiangyc/.vim/init.vim")

