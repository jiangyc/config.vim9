vim9script

import autoload "tui/util/cache.vim"

export def Init()
    if cache.Has("tui.command")
        return
    endif

    var tui_commands = {}
    cache.Set("tui.command", tui_commands)
    command! -nargs=+ -bang TuiCommand Exec(<args>)

    Push({ "id": "tui.command.list", "desc": "show all commands", "exec": function("List") })
enddef

export def Exec(cmd_id: string, cmd_args: list<any> = [])
    var tui_commands = cache.Get("tui.command", {})
    if !has_key(tui_commands, cmd_id)
        throw "tui-core-command_Exec: invalid command id [" .. cmd_id .. "]"
    endif

    var cmd_opts = tui_commands[cmd_id]
    var cmd_exec = cmd_opts["exec"]

    if type(cmd_exec) == v:t_string
        execute(cmd_exec)
    elseif type(cmd_exec) == v:t_func
        var TargetFunc = cmd_exec
        if empty(cmd_args)
            TargetFunc()
        else
            call(TargetFunc, cmd_args)
        endif
    else
        throw "tui-core-command_Exec: invalid exec"
    endif
enddef

export def Push(cmd_opts: dict<any>)
    if !cache.Has("tui.command")
        throw "tui-core-command_Push: please call Init() method first."
    endif
    var tui_commands = cache.Get("tui.command", {})

    var cmd_id = has_key(cmd_opts, "id") ? cmd_opts["id"] : ""
    if empty(cmd_id)
        throw "tui-core-command_Push: command id must not be an empty string."
    endif
    var cmd_exec = has_key(cmd_opts, "exec") ? cmd_opts["exec"] : ""
    if type(cmd_exec) == v:t_string && !empty(split(cmd_exec))
        # execute
    elseif type(cmd_exec) == v:t_func
        # call
    else
        throw "tui-core-command_Push: invalid exec"
    endif
    var cmd_desc = has_key(cmd_opts, "desc") ? cmd_opts["desc"] : ""

    tui_commands[cmd_id] = { "id": cmd_id, "exec": cmd_exec, "desc": cmd_desc }
    cache.Set("tui.command", tui_commands)
enddef
 
export def List()
    var tui_commands = cache.Get("tui.command", {})
    if empty(tui_commands)
        echo "empty"
        return
    endif

    for cmd_id in keys(tui_commands)
        var cmd_opts = tui_commands[cmd_id]
        echo cmd_id .. " => " cmd_opts["desc"]
    endfor
enddef
 
