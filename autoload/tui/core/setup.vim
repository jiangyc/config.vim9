vim9script

import autoload "tui/util/cache.vim"
import autoload "tui/core/layer.vim"
import autoload "tui/core/plug.vim"
import autoload "tui/core/command.vim" as comd

export def Setup()
    if cache.Get("tui.initialized", 0) == 1
        return
    endif
    cache.Set("tui.initialized", 1)

    # layer
    layer.Init()

    # plug
    plug.Init()

    # command
    comd.Init()
enddef
