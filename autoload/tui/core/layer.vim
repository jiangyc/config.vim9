vim9script

import autoload "tui/util/cache.vim"
import autoload "tui/util/path.vim"
import autoload "tui/util/loader.vim"

export def Load(layer_id: string): dict<string>
    if !cache.Has("tui.layer.home")
        throw "tui-core-layer_Load: you must call Init() first!"
    endif

    var layer_home = cache.Get("tui.layer.home")
    var layer_path = path.Join(layer_home, [substitute(layer_id, "\\.", path.SEP, "g")])
    var layer_info = {}
    if filereadable(layer_path .. ".vim")
        layer_info["id"] = layer_id
        layer_info["path"] = layer_path .. ".vim"
        layer_info["type"] = "file"
    elseif isdirectory(layer_path)
        layer_info["id"] = layer_id

        var layer_init_path = path.Join(layer_path, ["init.vim"])
        if filereadable(layer_init_path)
            layer_info["path"] = layer_init_path
            layer_info["type"] = "file"
        else
            layer_info["path"] = layer_path
            layer_info["type"] = "directory"
        endif
    endif

    if !empty(layer_info)
        var tui_layers = cache.Get("tui.layer.data", {})
        if !has_key(tui_layers, layer_id)
            tui_layers[layer_id] = layer_info
            loader.Load(layer_info["path"])
            cache.Set("tui.layer.data", tui_layers)
        endif
    endif
    return layer_info
enddef

export def Init(layer_home: string = "")
    if !cache.Has("tui.layer.home")
        var current_layer_home = path.Join(path.RTP, ["layers"])
        if !empty(layer_home)
            current_layer_home = layer_home
        endif
        cache.Set("tui.layer.home", current_layer_home)

        command! -nargs=1 -bang TuiLayer call Load(<f-args>)
        if isdirectory(current_layer_home)
            loader.Load(current_layer_home)
        endif
    endif
enddef
