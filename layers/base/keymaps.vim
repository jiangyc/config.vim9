vim9script

nnoremap <C-H> :bp<cr>
nnoremap <C-L> :bn<cr>
inoremap <C-H> <esc>:bp<cr>
inoremap <C-L> <esc>:bn<cr>

