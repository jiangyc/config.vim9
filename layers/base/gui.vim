vim9script

if !has("gui_running")
    finish
endif

set guioptions-=T
set guioptions+=m
if has("win32")
    set guifont=FiraCode\ Nerd\ Font:h11
else
    set guifont=FiraCode\ Nerd\ Font\ 11
endif
