vim9script

# ******************** line number ********************
set number
set relativenumber
# set numberwidth=2

# ******************** tab size ********************
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

set encoding=utf-8
set termencoding=utf-8
## vim.opt.fileencoding=utf-8
set fileencodings="utf-8,gb2312,gbk,ucs-bom,cp936,latin-1"

## ******************** blank chars ********************
set list
set listchars=space:·

## ******************** backup ********************
set nobackup
set nowritebackup
set noswapfile

## ******************** search ********************
set hlsearch
set ignorecase
set noincsearch

## ******************** filetype ********************
filetype on
filetype plugin on
filetype indent on

# ******************** colorscheme ********************
syntax on
if &t_Co == '256'
    set termguicolors
endif
colorscheme molokai

## ******************** others ********************
set nocompatible
set cursorline
set nowrap
set wildmenu
set clipboard+=unnamed
set conceallevel=0
set mouse=a

