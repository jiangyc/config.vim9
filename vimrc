vim9script

const current_path = expand("<sfile>:p:h")

# runtime path
if index(split(&rtp, ','), current_path) == -1
    execute("set rtp+=" .. current_path)
endif

# init
if filereadable(current_path .. "/init.vim")
    execute("source " .. current_path .. "/init.vim")
endif
